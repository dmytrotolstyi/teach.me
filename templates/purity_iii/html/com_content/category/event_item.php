<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
if(version_compare(JVERSION, '3.0', 'lt')){
	JHtml::_('behavior.tooltip');
}
JHtml::_('behavior.framework');

// Create a shortcut for params.
$location  = & $this->item->_location;
$contact  = & $this->item->_contact;
$extra_info  = & $this->item->_extra_info;
$created_by  = & $this->item->_created_by;
$startrepeat  = & $this->item->_startrepeat;
$endrepeat  = & $this->item->_endrepeat;
$description  = & $this->item->_description;
$content  = & $this->item->_content;
$title  = & $this->item->_title;
//$images  = json_decode($this->item->images);
$link_title = null;
$access_view=null;
$hasInfo = null;
$hasCtrl = null;

?>
<?php if ($this->item->state == 0) : ?>
<div class="system-unpublished">
	<?php endif; ?>

	<!-- Article -->
	<article>

		<!-- Intro image -->
		<div class="col-md-4">
			<?php if (isset($images->image_intro) and !empty($images->image_intro)) : 
				$current_image = $images->image_intro;
			else :
				$current_image = '/ja_purity/images/events/image_placeholder.jpg';
			endif; ?>
				<div class="pull-left item-image article-image article-image-intro">
					<img
						<?php /*if ($images->image_intro_caption):
							echo 'class="caption"' . ' title="' . htmlspecialchars($images->image_intro_caption) . '"';
						endif; */?>
						src="<?php echo htmlspecialchars($current_image); ?>"
						alt="<?php /*echo htmlspecialchars($images->image_intro_alt); */?>"
						class="event-img"/>
				</div>
		</div>

		<div class="col-md-8">
			<?php if ($title) : ?>
				<header class="article-header clearfix">
					<h2 class="article-title">
						<?php //if ($params->get('link_titles') && $params->get('access-view')) :
							if ($link_title && $access_view) : ?>
							<a href="<?php echo JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid)); ?>"> <?php echo $this->escape($title); ?></a>
						<?php else : ?>
							<?php echo $this->escape($title); ?>
						<?php endif; ?>
					</h2>
				</header>
			<?php endif; ?>

			<!-- Aside -->
			<?php if ($hasInfo || $hasCtrl) : ?>
				<aside class="article-aside clearfix">

					<?php // to do not that elegant would be nice to group the params ?>
					<?php if ($hasInfo) : ?>
						<?php echo JLayoutHelper::render('joomla.content.info_block.block', array('item' => $this->item, 'params' => $params, 'position' => 'above')); ?>
					<?php endif; ?>

					<?php if ($hasCtrl) : ?>
						<div class="btn-group pull-right">
							<a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="#"> <i class="fa fa-cog"></i> <span class="caret"></span></a>
							<ul class="dropdown-menu">

								<?php if ($params->get('show_print_icon')) : ?>
									<li class="print-icon"> <?php echo JHtml::_('icon.print_popup', $this->item, $params); ?> </li>
								<?php endif; ?>

								<?php if ($params->get('show_email_icon')) : ?>
									<li class="email-icon"> <?php echo JHtml::_('icon.email', $this->item, $params); ?> </li>
								<?php endif; ?>

								<?php if ($canEdit) : ?>
									<li class="edit-icon"> <?php echo JHtml::_('icon.edit', $this->item, $params); ?> </li>
								<?php endif; ?>
							</ul>
						</div>
					<?php endif; ?>
				</aside>
			<?php endif; ?>
			<!-- //Aside -->

			<section class="article-intro clearfix">
				<?php echo $description; ?>
			</section>

			<?php /*if ($params->get('show_readmore') && $this->item->readmore) :
				if ($params->get('access-view')) :
					$link = JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid));
				else :
					$menu      = JFactory::getApplication()->getMenu();
					$active    = $menu->getActive();
					$itemId    = $active->id;
					$link1     = JRoute::_('index.php?option=com_users&view=login&Itemid=' . $itemId);
					$returnURL = JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid));
					$link      = new JURI($link1);
					$link->setVar('return', base64_encode($returnURL));
				endif;*/
				?>
				<section class="readmore">
					<a class="btn btn-default" href="#">
						<span>
						<?php /*if (!$params->get('access-view')) :
							echo JText::_('COM_CONTENT_REGISTER_TO_READ_MORE');
						elseif ($readmore = $this->item->alternative_readmore) :
							echo $readmore;
							if ($params->get('show_readmore_title', 0) != 0) :
								echo JHtml::_('string.truncate', ($this->item->title), $params->get('readmore_limit'));
							endif;
						elseif ($params->get('show_readmore_title', 0) == 0) :*/
							echo JText::sprintf('COM_CONTENT_READ_MORE_TITLE');
						/*else :
							echo JText::_('COM_CONTENT_READ_MORE');
							echo JHtml::_('string.truncate', ($this->item->title), $params->get('readmore_limit'));
						endif;*/ ?>
						</span>
					</a>
				</section>
			<?php /*endif;*/ ?>
		</div>
	</article>
	<!-- //Article -->

	<?php if ($this->item->state == 0) : ?>
</div>
<?php endif; ?>

<?php /*echo $this->item->event->afterDisplayContent;*/ ?> 
